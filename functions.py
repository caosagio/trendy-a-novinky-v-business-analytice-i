def analyze_sentiment(text):
    # funkce pro zjistovani sentimentu vety
    from google.cloud import language_v1
    from google.oauth2 import service_account
    creds = service_account.Credentials.from_service_account_file('./credentials.json')
    client = language_v1.LanguageServiceClient(credentials=creds)
    language = "en"
    type_ = language_v1.Document.Type.PLAIN_TEXT
    encoding_type = language_v1.EncodingType.UTF8
    document = {"content": text, "type_": type_, "language": language}
    response = client.analyze_sentiment(request = {'document': document, 'encoding_type': encoding_type})
    return response

def analyze_entity(text):
    # funkce pro analyzu entit ve veze
    from google.cloud import language_v1
    from google.oauth2 import service_account
    creds = service_account.Credentials.from_service_account_file('./credentials.json')
    client = language_v1.LanguageServiceClient(credentials=creds)
    language = "en"
    type_ = language_v1.Document.Type.PLAIN_TEXT
    encoding_type = language_v1.EncodingType.UTF8
    document = {"content": text, "type_": type_, "language": language}
    response = client.analyze_entities(request = {'document': document, 'encoding_type': encoding_type})
    return response

def data_preprocess(data):
    print('Preprocessing data...')
    # funkce pro predzpracovani dat
    # vytvoreni vazeneho hodnoceni hotelu uzivatelem, vznikne hodnoceni s prihlednutim k sentimentu uzivatele
    # přičítáme 1, abychom se u nulového sentimentu vyhnuli násobení hodnocení uživatele 0
    df_sample2 = data.loc[:66051, :]
    df_sample2.loc[:, "sentiment_final"] = df_sample2["Pos_sentiment"] + df_sample2["Neg_sentiment"] 
    df_sample2.loc[:, "sentiment"] = df_sample2["Pos_sentiment"] + df_sample2["Neg_sentiment"] + 1
    df_sample2.loc[:, "Score_weighted"] = df_sample2["sentiment"] * df_sample2["Reviewer_Score"]
    
    df_for_filter = df_sample2 \
        .drop(columns = ['Tags', 'Review_Date', 'Additional_Number_of_Scoring', 'Reviewer_Score', \
                     'Neg_sentiment', 'Pos_sentiment', 'sentiment'])
    df_for_filter["Trip_type"] = df_for_filter["Trip_type"].str.strip()
    df_for_filter["Reviewer_Nationality"] = df_for_filter["Reviewer_Nationality"].str.strip()
    df_for_filter["Negative_Review"] = df_for_filter["Negative_Review"].str.strip()
    df_for_filter["Positive_Review"] = df_for_filter["Positive_Review"].str.strip()
    
    return df_for_filter

def review_selection(data):
    #######  parametry pro filtrovani ################
    # narodnost
    print("Selecting reviews based on your parameters...")
    df_selected = data
    nationality_final = ""
    nationality = input("Specify you nationality (Romania, Greece, Russia...): ")
    while (nationality_final == ""): 
        if nationality in data["Reviewer_Nationality"].tolist():
            nationality_final = nationality
        else: 
            nationality = input("We could not recognize your nationality. Try it again or answer 0 if you want to move to another question. ")
            if nationality in data["Reviewer_Nationality"].tolist() or nationality =="0":
                nationality_final = nationality
                
    if nationality_final != '0':
        df_selected = data.loc[data.Reviewer_Nationality == nationality_final, :]
                
    # mesic
    month_final = ""
    month = int(input("Specify the month of you trip (by number 1-12): "))
    while (month_final == ""): 
        if month in df_selected["Month"].tolist():
            month_final = month
        else: 
            month = int(input("We could not recognize month of your trip. Try it again or answer 0 if you want to move to another question. "))
            if month in df_selected["Month"].tolist() or month ==0:
                month_final = month
    
    if month_final != 0:
        df_selected = df_selected.loc[df_selected.Month == month_final, :]
                
    # typ vyletu
    triptype_final = ""
    triptype = input("Specify the type of your trip (Leisure or Business): ")
    while (triptype_final == ""): 
        if triptype in df_selected["Trip_type"].tolist():
            triptype_final = triptype
        else: 
            triptype = input("We could not recognize type of your trip. Try it again or answer 0 if you want to move to another question. ")
            if triptype in df_selected["Trip_type"].tolist() or triptype =="0":
                triptype_final = triptype

    if triptype_final != '0':
        df_selected = df_selected.loc[df_selected.Trip_type == triptype_final, :]
                
    # typ vyletu
    nightnum_final = ""
    nightnum = int(input("Specify the maximum number of nights (by number 1-5): "))
    while (nightnum_final == ""): 
        if nightnum in data["Nights_num"].tolist():
            nightnum_final = nightnum
        else: 
            nightnum = int(input("We could not recognize number of nights of your trip. Try it again or answer 0 if you want to move to another question. "))
            if nightnum in data["Nights_num"].tolist() or nightnum ==0:
                nightnum_final = nightnum
                
    if nightnum_final != 0:
        df_selected = df_selected.loc[df_selected.Nights_num <= nightnum_final, :]
       
    return df_selected

def hotels_selection(df_selected):
    import numpy as np
    # funkce na vyfiltrovani nejvhodnejsich hotelu na zaklade parametru od uzivatele
    # vyfiltruji se relevantni recenze hotelu podle parametru, vypocita se median vazeneho hodnoceni pro jednotlive relevantni hotely
    # vybere se 5 hotelu s nejvyssim vazenym hodnocenim
    print('Showing top 5 hotels based on your parameters...')
    df_aggregate = df_selected \
        .groupby('Hotel_Name') \
        .agg({'Score_weighted':np.median, "Negative_Review":'size'}) \
        .rename(columns={'Negative_Review' : 'Count'}) \
        .reset_index() \
        .sort_values(by=['Score_weighted'], ascending = False)
    df_aggregate = df_aggregate.loc[df_aggregate.Count > 10, :] \
        .drop(columns = ['Count']) \
        .head(5)
    return df_aggregate

def review_similarity(df_aggregate, df_selected):
    import spacy
    import pandas as pd
    # vytvoreni mapy podobnosti pro jeden vybrany hotel uzivatelem (vybira se ze seznamu nejvhodnejsich kandidatu)
    # mapa se vytvari zvlast pro pozitivni a negativni recenze
    print('Creating similarity map of reviews based on your hotel selection...')
    hotel_name_final = ""
    hotel_names = df_aggregate['Hotel_Name'].tolist()
    hotel_name = input("Select hotel name from recommended hotels {}: ".format(hotel_names))
    while (hotel_name_final == ""): 
        if hotel_name in df_aggregate['Hotel_Name'].tolist():
            hotel_name_final = hotel_name
        else: 
            hotel_name = input("We could not recognize your nationality. Please try it again.")
            if hotel_name in df_aggregate['Hotel_Name'].tolist():
                hotel_name_final = hotel_name
                
    df_selected_hotel = df_selected.loc[df_selected.Hotel_Name == hotel_name_final]
    nlp = spacy.load("en_core_web_md")
    docs = []
    for i in df_selected_hotel.index:
        if df_selected_hotel.loc[i, "Negative_Review"] != "No Negative":
            pom = nlp(df_selected_hotel.loc[i, "Negative_Review"])
            docs.append(pom)
    
    similarity_map_neg = pd.DataFrame()

    for doc in docs:
        for doc2 in docs:
            similarity_map_neg.loc[doc.text, doc2.text] = doc.similarity(doc2)
    
    docs = []
    for i in df_selected_hotel.index:
        if df_selected_hotel.loc[i, "Positive_Review"] != "No Positive":
            pom = nlp(df_selected_hotel.loc[i, "Positive_Review"])
            docs.append(pom)
    
    similarity_map_pos = pd.DataFrame()

    for doc in docs:
        for doc2 in docs:
            similarity_map_pos.loc[doc.text, doc2.text] = doc.similarity(doc2)
    return similarity_map_neg, similarity_map_pos, hotel_name_final

def data_for_entity_plot(df_for_filter, hotel_name_final):
    import numpy as np
    import pandas as pd
    df_for_image = df_for_filter.loc[(df_for_filter.Hotel_Name == hotel_name_final) & \
                                                         ((df_for_filter.Negative_Review != "No Negative") | \
                                                          (df_for_filter.Positive_Review != "No Positive")), :] \
        .head(50)
    
    df_entity = pd.DataFrame()
    d=0
    for i in range(0, df_for_image.shape[0]):
        sens = analyze_entity(str(df_for_image["Negative_Review"].iloc[i]).strip())
        for entity in sens.entities:
            df_entity.loc[d, "Hotel_Name"] = df_for_image["Hotel_Name"].iloc[i]
            df_entity.loc[d, "Entity_name"] = entity.name
            df_entity.loc[d, "Score"] = entity.salience
            d=d+1
        sens = analyze_entity(str(df_for_image["Positive_Review"].iloc[i]).strip())
        for entity in sens.entities:
            df_entity.loc[d, "Hotel_Name"] = df_for_image["Hotel_Name"].iloc[i]
            df_entity.loc[d, "Entity_name"] = entity.name
            df_entity.loc[d, "Score"] = -entity.salience
            d=d+1
            
    df_entity_aggregate = df_entity \
        .groupby(['Hotel_Name', 'Entity_name']) \
        .agg({'Score':np.median}) \
        .reset_index()
    
    df_entity_merged = df_entity[['Hotel_Name', 'Entity_name']] \
        .merge(df_entity_aggregate,  \
           on = ['Hotel_Name', 'Entity_name'], \
           how = 'left')
    
    return df_entity_merged

def plot_background(df_entity_merged):
    from wordcloud import (WordCloud, get_single_color_func)
    class GroupedColorFunc(object):
        """Create a color function object which assigns DIFFERENT SHADES of
            specified colors to certain words based on the color to words mapping.

           Uses wordcloud.get_single_color_func

           Parameters
           ----------
           color_to_words : dict(str -> list(str))
             A dictionary that maps a color to the list of words.

           default_color : str
             Color that will be assigned to a word that's not a member
             of any value from color_to_words.
        """

        def __init__(self, color_to_words, default_color):
            self.color_func_to_words = [
                (get_single_color_func(color), set(words))
                for (color, words) in color_to_words.items()]

            self.default_color_func = get_single_color_func(default_color)

        def get_color_func(self, word):
            """Returns a single_color_func associated with the word"""
            try:
                color_func = next(
                    color_func for (color_func, words) in self.color_func_to_words
                    if word in words)
            except StopIteration:
                color_func = self.default_color_func

            return color_func

        def __call__(self, word, **kwargs):
            return self.get_color_func(word)(word, **kwargs)
       
    color_to_words = {
        # words below will be colored with a green single color function
        '#00ff00': df_entity_merged.loc[df_entity_merged.Score > 0, "Entity_name"].unique().tolist(),
        # will be colored with a red single color function
        'red': df_entity_merged.loc[df_entity_merged.Score <= 0, "Entity_name"].unique().tolist()
    }
    default_color = 'grey'
    grouped_color_func = GroupedColorFunc(color_to_words, default_color)
    
    return grouped_color_func

def show_wordcloud(data, df_entity_merged, title = None):
    from wordcloud import WordCloud
    import matplotlib.pyplot as plt
    
    
    grouped_color_func = plot_background(df_entity_merged)
    wordcloud = WordCloud(
        background_color = 'white',
        max_words = 20,
        max_font_size = 40, 
        scale = 3,
        random_state = 42,
        regexp = r'\w[\w-]+'
    ).generate(str(data))
    wordcloud.recolor(color_func=grouped_color_func)
    fig = plt.figure(1, figsize = (20, 20))
    plt.axis('off')
    if title: 
        fig.suptitle(title, fontsize = 20)
        fig.subplots_adjust(top = 2.3)

    plt.imshow(wordcloud)
    plt.show()
    
    
def nationality_sentiment(data):
    df_nationality_agg = data \
        .groupby('Reviewer_Nationality') \
        .agg({'sentiment_final': 'mean', "Negative_Review":'size'}) \
        .rename(columns={'Negative_Review' : 'Count'}) \
        .reset_index() \
        .sort_values(by=['sentiment_final'], ascending = False)
    df_nationality_agg = df_nationality_agg.loc[(df_nationality_agg.Reviewer_Nationality != "") &
                                            (df_nationality_agg.Count > 0), :]
    df_nationality_pos = df_nationality_agg.head(5)
    df_nationality_neg = df_nationality_agg.tail(5)
    df_nationality = df_nationality_pos.append(df_nationality_neg)
    df_nationality.loc[df_nationality.sentiment_final< 0, 'Group'] = 'Negative'
    df_nationality.loc[df_nationality.sentiment_final> 0, 'Group'] = 'Positive'
          
    return df_nationality

def hotel_score(data):
    df_hotel_agg = data \
        .groupby('Hotel_Name') \
        .agg({'Score_weighted': 'mean', "Negative_Review":'size'}) \
        .rename(columns={'Negative_Review' : 'Count'}) \
        .reset_index() \
        .sort_values(by=['Score_weighted'], ascending = False)
    df_hotel_agg = df_hotel_agg.loc[df_hotel_agg.Count > 20, :]
    df_hotel_pos = df_hotel_agg.head(5)
    df_hotel_neg = df_hotel_agg.tail(5)
    df_hotel_score = df_hotel_pos.append(df_hotel_neg)
    df_hotel_score.loc[df_hotel_score.Score_weighted< 0, 'Group'] = 'Negative'
    df_hotel_score.loc[df_hotel_score.Score_weighted> 0, 'Group'] = 'Positive'
    
    return df_hotel_score
